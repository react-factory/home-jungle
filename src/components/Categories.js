import "../styles/Categories.css";
import { plantList } from "../datas/plantList";

function Categories({ selectedCategory, setSelectedCategory }) {
  const categories = plantList.reduce(
    (acc, plant) =>
      acc.includes(plant.category) ? acc : acc.concat(plant.category),
    []
  );
  return (
    <div className="lmj-categories">
      <div>
        <label for="pet-select">Categories: {selectedCategory}</label>
      </div>

      <div className="lmj-categories-select">
        <select
          name="categories"
          id="category-select"
          onChange={(e) => setSelectedCategory(e.target.value)}
        >
          <option value="">--Please choose a category--</option>

          {categories.map((cat) => (
            <option value={cat} key={cat}>
              {cat}
            </option>
          ))}
        </select>
      </div>
    </div>
  );
}

export default Categories;
