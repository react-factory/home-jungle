import { plantList } from "../datas/plantList";
import PlantItem from "./PlantItem";
import "../styles/ShoppingList.css";
import Categories from "./Categories";
import { useState } from "react";

function ShoppingList({ cart, updateCart }) {
  const [selectedCategory, setSelectedCategory] = useState(null);

  return (
    <div className="lmj-shopping-list">
      <Categories
        selectedCategory={selectedCategory}
        setSelectedCategory={setSelectedCategory}
      />

      <ul className="lmj-plant-list">
        {!selectedCategory
          ? plantList.map(({ id, cover, name, water, light, category }) => (
              <div key={id}>
                <PlantItem
                  cover={cover}
                  name={name}
                  water={water}
                  light={light}
                  category={category}
                />
                <button onClick={() => updateCart(cart + 1)}>Ajouter</button>
              </div>
            ))
          : plantList
              .filter((plant) => plant.category === selectedCategory)
              .map(({ id, cover, name, water, light, category }) => (
                <div key={id}>
                  <PlantItem
                    cover={cover}
                    name={name}
                    water={water}
                    light={light}
                    category={category}
                  />
                  <button onClick={() => updateCart(cart + 1)}>Ajouter</button>
                </div>
              ))}
      </ul>
    </div>
  );
}

export default ShoppingList;
